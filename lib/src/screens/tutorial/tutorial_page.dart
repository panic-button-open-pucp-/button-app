import 'package:flutter/material.dart';
import 'package:panic_app/src/screens/tutorial/widgets/info_widget.dart';
import 'package:panic_app/src/screens/tutorial/widgets/nav_widget.dart';
import 'package:panic_app/src/screens/tutorial/widgets/welcome_widget.dart';

class TutorialPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      color: Colors.blueGrey[900],
      child: Column(
        children: <Widget>[
          WelcomeWidget(),
          InfoWidget(),
          NavWidget(),
        ],
      ),
    );
  }
}
