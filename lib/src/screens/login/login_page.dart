import 'package:flutter/material.dart';
import 'package:panic_app/src/screens/login/widgets/inputs_widget.dart';
import 'package:panic_app/src/screens/login/widgets/logo_widget.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: <Widget>[
          LogoWidget(),
          InputsWidget(),
        ],
      ),
    );
  }
}
