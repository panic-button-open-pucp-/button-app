import 'package:flutter/material.dart';
import 'package:panic_app/src/screens/login/login_page.dart';
import 'package:panic_app/src/screens/tutorial/tutorial_page.dart';
import 'package:panic_app/src/values/strings.dart' show Strings;

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: themeData(),
      title: Strings.app,
      home: Scaffold(
        body: SingleChildScrollView(child: LoginPage()),
      ),
    );
  }

  ThemeData themeData() {
    return ThemeData(
      primaryColor: Colors.deepPurple[600],
      accentColor: Colors.pink,
    );
  }
}
